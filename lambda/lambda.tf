# Simple AWS Lambda Terraform Example
# requires 'index.js' in the same directory
# to test: run `terraform plan`
# to deploy: run `terraform apply`

variable "module_source_file" {
  type = string
}

variable "module_variables" {
  type    = map(string)
}

variable "module_function_name" {
  type = string
}

variable "module_handler_name" {
  type = string
}

variable "module_iam_role" {
  type = string
}

data "archive_file" "lambda_zip" {
    type          = "zip"
    source_file   = var.module_source_file
    output_path   = "lambda_function.zip"
}

resource "aws_lambda_function" "test_lambda" {
  filename         = "lambda_function.zip"
  function_name    = var.module_function_name
  role             = var.module_iam_role
  handler          = var.module_handler_name
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  runtime          = "nodejs12.x"
  environment {
    variables = var.module_variables
  }
}
